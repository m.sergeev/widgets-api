package application.repository;

import application.entity.WidgetEntity;
import application.model.request.WidgetModel;
import application.model.shape.Point;
import application.storage.MemoryWidgetStorage;
import application.storage.WidgetStorage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

@SpringBootTest
public class WidgetRepositoryIntegrationTest {
    @Autowired
    private WidgetStorage widgetStorage;

    @Autowired
    private WidgetRepository widgetRepository;

    @Test
    public void createWidgetTest() {
        WidgetModel widgetModel = new WidgetModel(new Point(1, 2), 10, 20, 15);

        WidgetEntity widgetEntity = this.widgetRepository.addNew(widgetModel);
        String uuid = widgetEntity.getId();

        WidgetEntity insertedEntity = this.widgetRepository.getByUuid(uuid);

        assert widgetEntity == insertedEntity;
    }

    @Test
    public void updateWidgetTest() {
        WidgetModel widgetModel = new WidgetModel(new Point(1, 2), 10, 20, 15);

        WidgetEntity widgetEntity = this.widgetRepository.addNew(widgetModel);
        String uuid = widgetEntity.getId();
        assert this.widgetRepository.getByUuid(uuid).getZIndex() == 15;

        WidgetModel newWidgetModel = new WidgetModel(new Point(5, 2), 8, 7, 10);
        this.widgetRepository.updateWidget(uuid, newWidgetModel);
        assert this.widgetRepository.getByUuid(uuid).getZIndex() == 10;
        assert this.widgetRepository.getByUuid(uuid).getHeight() == 8;
        assert this.widgetRepository.getByUuid(uuid).getWidth() == 7;
        assert this.widgetRepository.getByUuid(uuid).getPosition().getX() == 5;
    }

    @Test
    public void deleteWidgetTest() {
        WidgetModel widgetModel = new WidgetModel(new Point(1, 2), 10, 20, 15);

        WidgetEntity widgetEntity = this.widgetRepository.addNew(widgetModel);
        String uuid = widgetEntity.getId();
        assert this.widgetRepository.getByUuid(uuid).getZIndex() == 15;

        this.widgetRepository.deleteByUuid(uuid);
        assert this.widgetRepository.getByUuid(uuid) == null;
    }

    @Test
    public void getWidgetsTest() {
        WidgetModel widgetModel1 = new WidgetModel(new Point(1, 2), 10, 20, 15);
        WidgetModel widgetModel2 = new WidgetModel(new Point(1, 3), 11, 21, 16);
        WidgetModel widgetModel3 = new WidgetModel(new Point(1, 4), 12, 22, 15);

        WidgetEntity widgetEntity1 = this.widgetRepository.addNew(widgetModel1);
        WidgetEntity widgetEntity2 = this.widgetRepository.addNew(widgetModel2);
        WidgetEntity widgetEntity3 = this.widgetRepository.addNew(widgetModel3);

        ArrayList<WidgetEntity> widgets = new ArrayList<>(this.widgetRepository.getWidgets(0, 0));
        assert widgets.get(0).getZIndex() == 15;
        assert widgets.get(1).getZIndex() == 16;
        assert widgets.get(2).getZIndex() == 17;
        assert widgets.size() == 3;

        widgets = new ArrayList<>(this.widgetRepository.getWidgets(1, 2));
        assert widgets.get(0).getZIndex() == 17;
        assert widgets.size() == 1;
    }

    @Before
    public void setUp() {
        this.widgetStorage = new MemoryWidgetStorage();
        this.widgetRepository = new WidgetRepository(widgetStorage);
    }

    @After
    public void tearDown() {
        this.widgetStorage = null;
        this.widgetRepository = null;
    }
}
