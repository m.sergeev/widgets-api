package application.controller;

import application.model.request.WidgetModel;
import application.model.shape.Point;
import application.storage.WidgetStorage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class WidgetControllerIntegrationTest {
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private MockMvc mvc;
    @Autowired
    private WidgetStorage widgetStorage;

    @Test()
    public void testCreateWidget() throws Exception {
        String widgetJson = this.getWidgetJson(2, 3, 5, 5, 3);

        ResultActions result = this.sendPostRequest("/widgets", widgetJson);

        result.andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        String responseBody = result.andReturn().getResponse().getContentAsString();
        JsonNode node = this.objectMapper.readTree(responseBody);
        String height = node.get("height").asText();
        String width = node.get("width").asText();
        String zIndex = node.get("zindex").asText();

        assert height.equals("5");
        assert width.equals("5");
        assert zIndex.equals("3");
    }

    @Test()
    public void testCreateTopWidget() throws Exception {
        String widgetJson = this.getWidgetJson(2, 3, 5, 5, null);
        this.sendPostRequest("/widgets", widgetJson);


        widgetJson = this.getWidgetJson(2, 3, 5, 5, null);
        ResultActions result = this.sendPostRequest("/widgets", widgetJson);

        result.andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        String responseBody = result.andReturn().getResponse().getContentAsString();
        JsonNode node = this.objectMapper.readTree(responseBody);
        String height = node.get("height").asText();
        String width = node.get("width").asText();
        String zIndex = node.get("zindex").asText();

        assert height.equals("5");
        assert width.equals("5");
        assert zIndex.equals("2");
    }

    @Test()
    public void testAddAndGetWidget() throws Exception {
        String widgetJson = this.getWidgetJson(2, 3, 5, 5, null);

        ResultActions result = this.sendPostRequest("/widgets", widgetJson);
        result.andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        String responseBody = result.andReturn().getResponse().getContentAsString();
        JsonNode node = this.objectMapper.readTree(responseBody);
        String id = node.get("id").asText();

        ResultActions getResult = this.sendGetRequest("/widgets/" + id);
        getResult.andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        String getResponseBody = getResult.andReturn().getResponse().getContentAsString();
        JsonNode getNode = this.objectMapper.readTree(responseBody);

        Assert.assertEquals(id, node.get("id").asText());
        Assert.assertEquals(responseBody, getResponseBody);
    }

    @Test()
    public void testGetWidgetNotFound() throws Exception {
        String widgetJson = this.getWidgetJson(2, 3, 5, 5, null);

        ResultActions result = this.sendPostRequest("/widgets", widgetJson);
        result.andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        ResultActions getResult = this.sendGetRequest("/widgets/" + "incorrect-id");
        getResult.andExpect(status().is(404))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test()
    public void testGetAllWidgets() throws Exception {
        int startCount = widgetStorage.count();

        String widgetJson = this.getWidgetJson(2, 3, 5, 5, null);

        this.sendPostRequest("/widgets", widgetJson).andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
        this.sendPostRequest("/widgets", widgetJson).andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
        this.sendPostRequest("/widgets", widgetJson).andExpect(status().is(201))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        ResultActions getResult = this.sendGetRequest("/widgets?limit=2&offset=2");
        getResult.andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        JsonNode node = this.objectMapper.readTree(getResult.andReturn().getResponse().getContentAsString());

        int expectedRetrieved = startCount == 0 ? 1 : 2;
        Assert.assertEquals(expectedRetrieved, node.get("retrieved").asInt());
        Assert.assertEquals(startCount + 3, node.get("totalCount").asInt());
    }

    private ResultActions sendPostRequest(String route, String requestBody) throws Exception {
        return mvc.perform(post(route)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));
    }

    private ResultActions sendGetRequest(String route) throws Exception {
        return mvc.perform(get(route)
                .contentType(MediaType.APPLICATION_JSON));
    }

    private ResultActions sendPutRequest(String route, String requestBody) throws Exception {
        return mvc.perform(put(route)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));
    }

    private ResultActions sendDeleteRequest(String route, String requestBody) throws Exception {
        return mvc.perform(delete(route)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody));
    }

    private String getWidgetJson(int x, int y, int height, int width, Integer zIndex) throws JsonProcessingException {
        WidgetModel widgetModel = new WidgetModel(new Point(x, y), height, width, zIndex);
        return this.objectMapper.writeValueAsString(widgetModel);
    }
}
