package application.entity;

import application.model.shape.Point;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class WidgetEntity {
    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date updatedAt;

    private Point position;

    private int height;
    private int width;

    private int zIndex;

    public WidgetEntity(String id, Point position, int height, int width, int zIndex) {
        this.id = id;
        this.position = position;
        this.height = height;
        this.width = width;
        this.zIndex = zIndex;
        this.updatedAt = new Date();
    }

    public String getId() {
        return this.id;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public Point getPosition() {
        return position;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getZIndex() {
        return zIndex;
    }

    public Void incrementZIndex() {
        this.zIndex++;
        this.updatedAt = new Date();
        return null;
    }
}
