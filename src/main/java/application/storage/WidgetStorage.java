package application.storage;

import application.entity.WidgetEntity;

import java.util.Collection;

public interface WidgetStorage {
    Collection<WidgetEntity> getAll(int limit, int offset);

    Collection<WidgetEntity> getAll();

    WidgetEntity getByUuid(String id);

    WidgetEntity deleteByUuid(String id);

    void insert(WidgetEntity widgetEntity);

    void update(WidgetEntity widgetEntity);

    int count();

    WidgetEntity getLast();
}
