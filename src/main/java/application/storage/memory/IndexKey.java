package application.storage.memory;

//Note: this class has a natural ordering that is inconsistent with equals.
public class IndexKey implements Comparable {
    private int zIndex;

    public IndexKey(int zIndex) {
        this.zIndex = zIndex;
    }

    public int getZIndex() {
        return zIndex;
    }

    public void incrementZIndex() {
        zIndex++;
    }

    @Override
    public int compareTo(Object o) {
        int zIndex = ((IndexKey) o).getZIndex();
        return Integer.compare(this.zIndex, zIndex);
    }
}
