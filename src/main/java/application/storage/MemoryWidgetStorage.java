package application.storage;

import application.entity.WidgetEntity;
import application.storage.memory.IndexKey;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Stream;

@Component
public class MemoryWidgetStorage implements WidgetStorage {
    private final TreeMap<IndexKey, WidgetEntity> widgetStorage = new TreeMap<>();
    private final HashMap<String, IndexKey> keyIndexPairStorage = new HashMap<>();

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock readLock = lock.readLock();
    private final Lock writeLock = lock.writeLock();

    @Override
    public Collection<WidgetEntity> getAll() {
        this.readLock.lock();
        Collection<WidgetEntity> widgets = this.widgetStorage.values();
        this.readLock.unlock();

        return widgets;
    }

    @Override
    public Collection<WidgetEntity> getAll(int limit, int offset) {
        this.readLock.lock();

        Stream widgetStream = this.widgetStorage.values().stream();
        if (offset > 0) {
            widgetStream = widgetStream.skip(offset);
        }
        if (limit > 0) {
            widgetStream = widgetStream.limit(limit);
        }
        LinkedList widgets = new LinkedList<WidgetEntity>();
        widgetStream.forEach(widgets::add);

        this.readLock.unlock();

        return widgets;
    }

    @Override
    public WidgetEntity getByUuid(String uuid) {
        WidgetEntity widget = null;

        this.readLock.lock();

        IndexKey indexKey = this.keyIndexPairStorage.get(uuid);
        if (indexKey != null) {
            widget = this.widgetStorage.get(indexKey);
        }

        this.readLock.unlock();

        return widget;
    }

    @Override
    public WidgetEntity deleteByUuid(String uuid) {
        WidgetEntity widget = null;

        this.writeLock.lock();

        IndexKey indexKey = this.keyIndexPairStorage.get(uuid);
        if (indexKey != null) {
            this.keyIndexPairStorage.remove(uuid);
            widget = this.widgetStorage.remove(indexKey);
        }

        this.writeLock.unlock();

        return widget;
    }

    @Override
    public void insert(WidgetEntity widgetEntity) {
        IndexKey key = new IndexKey(widgetEntity.getZIndex());

        this.writeLock.lock();

        this.shiftZIndicesFrom(key);
        this.widgetStorage.put(key, widgetEntity);
        this.keyIndexPairStorage.put(widgetEntity.getId(), key);

        this.writeLock.unlock();
    }

    @Override
    public void update(WidgetEntity widgetEntity) {
        IndexKey key = new IndexKey(widgetEntity.getZIndex());

        this.writeLock.lock();

        IndexKey oldKey = this.keyIndexPairStorage.get(widgetEntity.getId());
        this.shiftZIndicesFrom(key);
        this.keyIndexPairStorage.put(widgetEntity.getId(), key);
        this.widgetStorage.remove(oldKey);
        this.widgetStorage.put(key, widgetEntity);

        this.writeLock.unlock();
    }

    public int count() {
        this.readLock.lock();
        int count = this.widgetStorage.size();
        this.readLock.unlock();

        return count;
    }

    public WidgetEntity getLast() {
        this.readLock.lock();

        WidgetEntity widget = null;
        if (this.widgetStorage.size() > 0) {
            widget = this.widgetStorage.lastEntry().getValue();
        }

        this.readLock.unlock();

        return widget;
    }

    private void shiftZIndicesFrom(IndexKey indexKey) {
        NavigableMap<IndexKey, WidgetEntity> tail = this.widgetStorage.tailMap(indexKey, true);
        NavigableSet<IndexKey> keys = tail.navigableKeySet();
        Collection<WidgetEntity> widgets = tail.values();

        int currentZIndex = indexKey.getZIndex();
        int widgetsIncremented = 0;
        for (WidgetEntity widgetEntity : widgets) {
            if (currentZIndex < widgetEntity.getZIndex()) {
                break;
            }
            if (currentZIndex == widgetEntity.getZIndex()) {
                widgetEntity.incrementZIndex();
                widgetsIncremented++;
            }
            currentZIndex = widgetEntity.getZIndex();
        }

        currentZIndex = indexKey.getZIndex();
        int keysIncremented = 0;
        for (IndexKey key : keys) {
            if (currentZIndex == key.getZIndex()) {
                key.incrementZIndex();
                keysIncremented++;
            }
            currentZIndex = key.getZIndex();
            if (keysIncremented == widgetsIncremented) {
                break;
            }
        }
    }
}
