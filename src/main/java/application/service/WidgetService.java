package application.service;

import application.entity.WidgetEntity;
import application.model.request.WidgetModel;
import application.model.shape.Point;
import application.model.shape.Rectangle;
import application.repository.WidgetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class WidgetService {

    @Autowired
    private WidgetRepository widgetRepository;

    public WidgetEntity createWidget(WidgetModel widgetModel) {
        if (widgetModel.getZIndex() == null) {
            int topZIndex = this.widgetRepository.getTopZIndex();
            widgetModel.setZIndex(topZIndex + 1);
        }
        return this.widgetRepository.addNew(widgetModel);
    }

    public Collection<WidgetEntity> getWidgets(int limit, int offset) {
        return this.widgetRepository.getWidgets(limit, offset);
    }

    public ArrayList<WidgetEntity> getWidgets(Point start, Point end, int limit, int offset) {
        Collection<WidgetEntity> widgets = this.widgetRepository.getWidgets();
        ArrayList<WidgetEntity> filteredWidgets = widgets.stream()
                .filter(widget -> this.isWidgetInArea(widget, start, end))
                .skip(offset)
                .limit(limit)
                .collect(Collectors.toCollection(ArrayList::new));
        return filteredWidgets;
    }

    public WidgetEntity getWidgetByUuid(String uuid) {
        return this.widgetRepository.getByUuid(uuid);
    }

    public WidgetEntity updateWidgetByUuid(String uuid, WidgetModel widgetModel) {
        if (this.widgetRepository.getByUuid(uuid) == null) {
            return null;
        }
        return this.widgetRepository.updateWidget(uuid, widgetModel);
    }

    public WidgetEntity deleteWidgetByUuid(String uuid) {
        return this.widgetRepository.deleteByUuid(uuid);
    }

    public int getWidgetsCount() {
        return this.widgetRepository.getWidgetsCount();
    }

    private boolean isWidgetInArea(WidgetEntity widgetEntity, Point leftBottom, Point rightTop) {
        Rectangle filterArea = new Rectangle(leftBottom, rightTop);
        Rectangle widgetArea = new Rectangle(widgetEntity.getWidth(), widgetEntity.getHeight(), widgetEntity.getPosition());

        return widgetArea.isIncludedIn(filterArea);
    }
}
