package application.controller;

import application.entity.WidgetEntity;
import application.exception.EntityNotFoundException;
import application.model.request.FilterArea;
import application.model.request.WidgetModel;
import application.model.response.EntityNotFoundResponse;
import application.model.response.ValidationErrorResponse;
import application.model.response.WidgetCollectionResponse;
import application.model.validation.ValidationError;
import application.service.WidgetService;
import application.validator.ExistingWidget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("/widgets")
public class WidgetController {

    @Autowired
    private WidgetService widgetService;

    @GetMapping
    public ResponseEntity getWidgets(
            @RequestParam(defaultValue = "10") int limit,
            @RequestParam(defaultValue = "0") int offset,
            @Valid FilterArea filterArea,
            Errors errors
    ) {
        Collection<WidgetEntity> widgets;
        if (errors.hasErrors()) {
            return ResponseEntity
                    .unprocessableEntity()
                    .body(this.createValidationErrorResponse(errors));
        }
        if (filterArea.isCompleteArea()) {
            widgets = this.widgetService.getWidgets(filterArea.getStart(), filterArea.getEnd(), limit, offset);
        } else {
            widgets = this.widgetService.getWidgets(limit, offset);
        }

        int totalCount = this.widgetService.getWidgetsCount();
        return ResponseEntity.ok(new WidgetCollectionResponse(widgets, totalCount));
    }

    @GetMapping("/{uuid}")
    public ResponseEntity getWidget(@PathVariable String uuid) throws EntityNotFoundException {
        WidgetEntity widgetEntity = widgetService.getWidgetByUuid(uuid);
        if (widgetEntity == null) {
            return ResponseEntity.
                    status(HttpStatus.NOT_FOUND)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(new EntityNotFoundResponse());
        }
        return ResponseEntity.ok(widgetEntity);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity addWidget(@Valid @RequestBody WidgetModel widgetModel, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntity
                    .unprocessableEntity()
                    .body(this.createValidationErrorResponse(errors));
        }

        WidgetEntity widgetEntity = widgetService.createWidget(widgetModel);

        return new ResponseEntity<>(widgetEntity, HttpStatus.CREATED);
    }

    @PutMapping("/{uuid}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity updateWidget(@PathVariable String uuid, @Validated({ExistingWidget.class}) @RequestBody WidgetModel widget, Errors errors) throws EntityNotFoundException {
        if (errors.hasErrors()) {
            return ResponseEntity
                    .unprocessableEntity()
                    .body(this.createValidationErrorResponse(errors));
        }

        WidgetEntity widgetEntity = widgetService.updateWidgetByUuid(uuid, widget);
        if (widgetEntity == null) {
            throw new EntityNotFoundException();
        }

        return ResponseEntity.ok(widgetEntity);
    }

    @DeleteMapping("/{uuid}")
    @ResponseStatus(HttpStatus.OK)
    public WidgetEntity deleteWidget(@PathVariable String uuid) throws EntityNotFoundException {
        WidgetEntity widgetEntity = this.widgetService.deleteWidgetByUuid(uuid);
        if (widgetEntity == null) {
            throw new EntityNotFoundException();
        }

        return widgetEntity;
    }

    private ValidationErrorResponse createValidationErrorResponse(Errors errors) {
        ArrayList<ValidationError> validationErrors = new ArrayList<>();
        for (FieldError error : errors.getFieldErrors()) {
            ValidationError validationError = new ValidationError(error.getField(), error.getDefaultMessage());
            validationErrors.add(validationError);
        }
        for (ObjectError error : errors.getGlobalErrors()) {
            ValidationError validationError = new ValidationError(error.getObjectName(), error.getDefaultMessage());
            validationErrors.add(validationError);
        }
        return new ValidationErrorResponse(validationErrors);
    }
}
