package application.repository;

import application.entity.WidgetEntity;
import application.model.request.WidgetModel;
import application.storage.WidgetStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.UUID;

@Repository
public class WidgetRepository {

    @Autowired
    private WidgetStorage widgetStorage;

    public WidgetRepository(WidgetStorage widgetStorage) {
        this.widgetStorage = widgetStorage;
    }

    public WidgetEntity addNew(WidgetModel widgetModel) {
        String uuid = UUID.randomUUID().toString();
        WidgetEntity widgetEntity = widgetModel.toEntity(uuid);

        this.widgetStorage.insert(widgetEntity);
        return widgetEntity;
    }

    public WidgetEntity updateWidget(String uuid, WidgetModel widgetModel) {

        WidgetEntity widgetEntity = widgetModel.toEntity(uuid);

        this.widgetStorage.update(widgetEntity);
        return widgetEntity;
    }

    public Collection<WidgetEntity> getWidgets() {
        return this.widgetStorage.getAll();
    }

    public Collection<WidgetEntity> getWidgets(int limit, int offset) {
        return this.widgetStorage.getAll(limit, offset);
    }

    public WidgetEntity getByUuid(String uuid) {
        return this.widgetStorage.getByUuid(uuid);
    }

    public WidgetEntity deleteByUuid(String uuid) {
        return this.widgetStorage.deleteByUuid(uuid);
    }

    public int getWidgetsCount() {
        return this.widgetStorage.count();
    }

    public int getTopZIndex() {
        WidgetEntity widget = this.widgetStorage.getLast();
        if (widget != null) {
            return widget.getZIndex();
        }
        return 0;
    }
}
