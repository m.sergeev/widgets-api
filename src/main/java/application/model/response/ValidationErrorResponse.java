package application.model.response;

import application.model.validation.ValidationError;

import java.util.ArrayList;

public class ValidationErrorResponse {
    private ArrayList<ValidationError> errors;

    public ValidationErrorResponse(ArrayList<ValidationError> errors) {
        this.errors = errors;
    }

    public ArrayList<ValidationError> getErrors() {
        return this.errors;
    }

}
