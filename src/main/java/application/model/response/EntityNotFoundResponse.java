package application.model.response;

public class EntityNotFoundResponse {
    private final String error = "Entity not found";

    public String getError() {
        return error;
    }
}
