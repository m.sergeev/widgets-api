package application.model.response;

import application.entity.WidgetEntity;

import java.util.Collection;

public class WidgetCollectionResponse {
    private int retrieved;
    private int totalCount;
    private Collection<WidgetEntity> widgets;

    public WidgetCollectionResponse(Collection<WidgetEntity> widgets, int totalCount) {
        this.retrieved = widgets.size();
        this.totalCount = totalCount;
        this.widgets = widgets;
    }

    public int getRetrieved() {
        return retrieved;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public Collection<WidgetEntity> getWidgets() {
        return widgets;
    }
}
