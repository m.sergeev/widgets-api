package application.model.request;

import application.model.shape.Point;
import application.validator.FilterAreaRange;
import application.validator.IsFilterAreaRequired;

@FilterAreaRange
@IsFilterAreaRequired
public class FilterArea {
    private Integer startX;
    private Integer startY;

    private Integer endX;
    private Integer endY;

    private Point start;
    private Point end;

    public FilterArea(Integer startX, Integer startY, Integer endX, Integer endY) {
        this.startX = startX;
        this.endX = endX;
        this.startY = startY;
        this.endY = endY;
        this.start = new Point(this.startX, this.startY);
        this.end = new Point(this.endX, this.endY);
    }

    public Point getStart() {
        return this.start;
    }

    public Point getEnd() {
        return this.end;
    }

    public boolean isCompleteArea() {
        return this.startX != null && this.startY != null && this.endX != null && this.endY != null;
    }

    public boolean isEmptyArea() {
        return this.startX == null && this.startY == null && this.endX == null && this.endY == null;
    }
}
