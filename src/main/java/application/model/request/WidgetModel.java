package application.model.request;

import application.entity.WidgetEntity;
import application.model.shape.Point;
import application.validator.ExistingWidget;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class WidgetModel {
    @Valid
    @NotNull(message = "Position must not be empty")
    private Point position;

    @NotNull(message = "Height must not be empty")
    @Range(
            min = 0,
            message = "Height must be greater than zero"
    )
    private Integer height;

    @NotNull(message = "Width must not be empty")
    @Range(
            min = 0,
            message = "Width must be greater than zero"
    )
    private Integer width;

    @NotNull(message = "Z Index must not be empty", groups = ExistingWidget.class)
    private Integer zIndex;

    public WidgetModel(Point position, Integer height, Integer width, Integer zIndex) {
        this.position = position;
        this.height = height;
        this.width = width;
        this.zIndex = zIndex;
    }

    public WidgetEntity toEntity(String id) {
        return new WidgetEntity(id, this.position, this.height, this.width, this.zIndex);
    }

    public Point getPosition() {
        return position;
    }

    public Integer getHeight() {
        return height;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getZIndex() {
        return zIndex;
    }

    public void setZIndex(Integer value) {
        this.zIndex = value;
    }
}
