package application.model.shape;

public class Rectangle {
    private Point start;

    private Point end;

    public Rectangle(int width, int height, Point start) {
        this.start = start;
        this.end = new Point(
                start.getX() + width,
                start.getY() + height
        );
    }

    public Rectangle(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    public boolean isIncludedIn(Rectangle rectangle) {
        return this.start.getX() <= rectangle.end.getX() && this.start.getX() >= rectangle.start.getX()
                && this.start.getY() <= rectangle.end.getY() && this.start.getY() >= rectangle.start.getY()
                && this.end.getX() >= rectangle.start.getX() && this.end.getX() <= rectangle.end.getX()
                && this.end.getY() >= rectangle.start.getY() && this.end.getY() <= rectangle.end.getY();
    }
}
