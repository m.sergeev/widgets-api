package application.model.shape;

import javax.validation.constraints.NotNull;

public class Point {
    @NotNull(message = "X must not be empty")
    private Integer x;

    @NotNull(message = "Y must not be empty")
    private Integer y;

    public Point(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }
}
