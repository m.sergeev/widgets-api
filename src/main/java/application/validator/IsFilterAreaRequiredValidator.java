package application.validator;

import application.model.request.FilterArea;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsFilterAreaRequiredValidator implements ConstraintValidator<IsFilterAreaRequired, FilterArea> {
    @Override
    public void initialize(IsFilterAreaRequired constraintAnnotation) {

    }

    @Override
    public boolean isValid(FilterArea filterArea, ConstraintValidatorContext constraintValidatorContext) {
        return filterArea.isCompleteArea() || filterArea.isEmptyArea();
    }
}
