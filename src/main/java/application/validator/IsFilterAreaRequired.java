package application.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = IsFilterAreaRequiredValidator.class)
public @interface IsFilterAreaRequired {
    String message() default "All four Filter Area parameters must be filled in query parameters: startX, endX, startY, endY";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}