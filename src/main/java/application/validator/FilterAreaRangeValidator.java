package application.validator;

import application.model.request.FilterArea;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FilterAreaRangeValidator implements ConstraintValidator<FilterAreaRange, FilterArea> {
    @Override
    public void initialize(FilterAreaRange constraintAnnotation) {

    }

    @Override
    public boolean isValid(FilterArea filterArea, ConstraintValidatorContext constraintValidatorContext) {
        if (!filterArea.isCompleteArea()) {
            return true;
        }

        boolean isXCorrect = filterArea.getStart().getX() < filterArea.getEnd().getX();
        boolean isYCorrect = filterArea.getStart().getY() < filterArea.getEnd().getY();

        return isXCorrect && isYCorrect;
    }
}
