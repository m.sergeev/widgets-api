package application.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = FilterAreaRangeValidator.class)
public @interface FilterAreaRange {
    String message() default "Start X/Y must be less than End X/Y in order to form a proper Filter Area";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
