# Widgets API

This is a web application which allows performing CRUD operations on widgets.

Widgets exist in (kind of) a two-dimensional board and have the following properties:

1. Position - X and Y (int).
2. Height (int, greater than 0).
3. Width (int, greate than 0).
4. Z Index (int).
Widgets also each have a unique identifier (UUID), which is used in some CRUD operations.

## API Reference
All the interactions are done using HTTP and Json media type.

Swagger spec is available at GET /swagger-ui, but it is a bit weird so there is the quick API reference:

### POST /widgets.
This method adds a new widget to the board.
WidgetModel is used to validate and process the request body e.g.

  {
  	"position": {
  		"x": 0,
  		"y": 0
  	},
  	"height": 5,
  	"width": 5,
  	"zindex": 0
  }

  
If "zindex" is not specified, then the widget gets places above every other.

If a widget with the same "zindex" already exists, then new widget takes the "zindex", while the old one gets its "zindex" reduced by 1. The reductions are then done to every other conflicting widget, as "zindex"-1 could be taken too.

Possible status codes:
* 201 Created - widget successfully added.
* 422 Unprocessable Entity - widget in request body contains invalid values.
 

### GET /widget/{id}
This method allows getting the single widget's data using its ID.

Nothing fancy about it.

Possible status codes:
* 200 OK.
* 404 Not Found - widget with given ID does not exist on the board.

### GET /widgets
This method allows getting multiple widgets in single request. 

There are *pagination* and *filtering* options available.
1. Pagination is done using *limit* and *offset* query parameters. Default limit is 10. /widgets?limit=10&offset=0. Props can be used separately.
2. Filtering allows only getting widgets in given XY area. The area bounds should also be passed as query parameters because it does not feel right to pass request body to GET methods. Filtering bounds should be named *startX, startY, endX, endY* in query parameters. They are also *integers*.

Response contains retrieved count, total count and the list of selected widgets.

Possible status codes:
* 200 OK.
* 422 Unprocessable Entity - filtering bounds are invalid or only part of the bound is filled.

### PUT /widgets/{id}
Updates widget with given id using request body e.g.   {
                                                       	"position": {
                                                       		"x": 0,
                                                       		"y": 0
                                                       	},
                                                       	"height": 5,
                                                       	"width": 5,
                                                       	"zindex": 0
                                                       }.
Unlike using POST /widgets, "zindex" is required to update a widget, but the shifting rules also apply.

Possible status codes:
* 200 OK.
* 404 Not Found - widget with given id does not exist on the board.
* 422 Unprocessable Entity - widget in request body contains invalid values.

### DELETE /widgets/{id}
Deletes widget with given id from the board.

Possible status codes:
* 200 OK.
* 404 Not Found - widget with given ID does not exist on the board.
                                                      